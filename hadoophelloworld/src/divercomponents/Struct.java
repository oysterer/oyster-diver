package hadoophelloworld;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

public class Struct implements Writable, WritableComparable<Struct> {
String recid,fname,lname,mname,address,city,state,zip,ssn,homephone;
	
	public Struct(){
	}
	
	public Struct(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8,String s9, String s10){
		recid=s1;
		fname=s2;
		lname=s3;
		mname=s4;
		address=s5;
		city=s6;
		state=s7;
		zip=s8;
		ssn=s9;
		homephone=s10;
	}
	
	public void setRecid(String in){
		recid = in;
	}
	
	public void setFname(String in){
		fname = in;
	}
	
	public void setLname(String in){
		lname = in;
	}
	
	public void setMname(String in){
		mname = in;
	}
	public void setAddress(String in){
		address = in;
	}
	public void setCity(String in){
		city = in;
	}
	public void setState(String in){
		state = in;
	}
	public void setZip(String in){
		zip = in;
	}
	public void setSsn(String in){
		ssn = in;
	}
	public void setHomephone(String in){
		homephone = in;
	}
	
	public String getRecid(){
		return recid;
	}
	public String getFname(){
		return fname;
	}
	public String getLname(){
		return lname;
	}
	public String getMname(){
		return mname;
	}
	public String getAddress(){
		return address;
	}
	public String getCity(){
		return city;
	}
	public String getState(){
		return state;
	}
	public String getZip(){
		return zip;
	}
	public String getSsn(){
		return ssn;
	}
	public String getHomephone(){
		return homephone;
	}

	@Override
	public int compareTo(Struct o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
	


	
	





	
}
