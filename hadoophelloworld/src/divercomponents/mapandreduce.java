package hadoophelloworld;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;





public class mapandreduce {
	
	static String key;
	static String Rule1;
	static String Rule2;
	static String Rule3;
	static String Rule4;
//	static String Rule1A;
//	static String Rule2A;
//	static String Rule3A;
//	static String Rule4A;
	static Soundex sdx = new Soundex();

	
 public static void main(String[] args) throws Exception {
		BufferedReader keyIn = new BufferedReader(new InputStreamReader(System.in));
		
		try
		{
			System.out.println("Enter name of pair file:");
			String fname = keyIn.readLine();
			File inFile = new File(fname);
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/huzaifasyed/Documents/workspace/input/rules.txt"));
			String[] temp = new String[11];
			String inputPair;
			while ((inputPair = br.readLine()) != null){
//			    StringTokenizer itr = new StringTokenizer(inputPair, "|");
				temp= inputPair.split("[|]",-1);
				if (temp.length==10){
				key= temp[0].toString().trim();
				Rule1= checkAlpha(temp[1]) +checkAlpha(temp[2]) ;
				Rule2 = checkAlpha(temp[2])+checkDigit(temp[8]);
				Rule3 = checkSoundex(temp[1])+checkSoundex(temp[2])+checkDigit(temp[8]);
				Rule4 = checkSoundex(temp[1])+checkSoundex(temp[2])+ checkAlphaNumeric(temp[4]);
//				Rule1A= temp[1].trim() +temp[2].trim() ;
//				Rule2A = temp[2].trim()+temp[8].trim();
//				Rule3A = temp[1]+temp[2].trim()+temp[8].trim();
//				Rule4A = temp[1].trim()+temp[2].trim()+ temp[4].trim();
				
				writer.write("Key:" + key+ " Rule 1: " + Rule1 + " Rule 2: " + Rule2 + " Rule 3:" + Rule3 + " Rule 4:" + Rule4);
				writer.write("\n");
//				writer.write("Key: " + key+ " Rule 1: " + Rule1A + " Rule 2: " + Rule2A + " Rule 3:" + Rule3A + " Rule 4:" + Rule4A);
//				writer.write("\n");

				}
				else {
					System.out.println("This line was not accepted: " + temp[0].toString());
				}
			    
			     
			
		}
			br.close();
			writer.close();
		}
		catch(IOException e)
		{
			System.out.println(e.toString());
		}
	  }
 
 public static String checkAlpha(String in) {
	 if(in.length() ==0 || in.matches("\\d+")) 
	 {
	 return key.trim();
	 }
	 else {
	 return in.replaceAll("[^A-Za-z]+", "").toUpperCase().trim();
	 }
 }

 public static String checkAlphaNumeric(String in) {
	 if(in.length() ==0 ) 
	 {
	 return key.trim();
	 }
	 else {
	 return in.replaceAll("[^A-Za-z0-9]+", "").toUpperCase().trim();
	 }
 }
 public static String checkDigit(String in) {
	 if(in.length() ==0 ) 
	 {
	 return key.trim();
	 }
	 else {
	 return in.replaceAll("\\D+", "").trim();
	 }
 }
 public static String checkSoundex(String in) {
	 if(in.length() ==0 || in.matches("\\d+")) 
	 {
	 return key.trim();
	 }
	 else {
	 return sdx.soundex(in.trim());
	 }
 }
 
 }



