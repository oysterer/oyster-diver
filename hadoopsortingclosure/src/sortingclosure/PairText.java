package sortingclosure;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

public class PairText implements Writable, WritableComparable<PairText> {
Text key, value;
	
	public PairText(){
	}
	
	public PairText(Text s1, Text s2){
		key = s1;
		value = s2;
	}
	
	public void setKey(Text in){
		key = new Text(in);
	}
	public void setValue(Text in){
		value = new Text(in);
	}
	public Text getKey(){
		return key;
	}
	public Text getValue(){
		return value;
	}
	
	public int compareTo(PairText cp){
		int result = key.compareTo(cp.getKey());
		if (result==0) {
			return value.compareTo(cp.getValue());
		} else {
			return result;
		}
	}
	
	public boolean isEqualTo(PairText pr){
		if ((this.key).equals(pr.getKey()) && (this.value).equals(pr.getValue())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean hasSameKeyAs(PairText pr){
		if ((this.key).equals(pr.getKey())) {
			return true;
		} else {
			return false;
		}	
	}
	
	public PairText reversePairText(){
		PairText result = new PairText();
		result.setKey(value);
		result.setValue(key);
		return result;
	}
	
	public Boolean keySameAsValue(){
		if(key.equals(value)) return true; else return false;
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
}
