package sortingclosure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;



public class Rules {
	public static class Regex
    extends Mapper<Object, Text, Text, Text>{
		static String key1;
		static String Rule1;
		static String Rule2;
		static String Rule3;
		static String Rule4;
		static Soundex sdx = new Soundex();

//		private Text ky = new Text();
//	    private Text val = new Text();
	    public void map(Object key, Text value, Context context) 
		 throws IOException, InterruptedException {
	    	String[] temp = new String[11];
//     StringTokenizer itr = new StringTokenizer(value.toString(), "\t");
	    	temp= value.toString().split("[\t]",-1);
		if (temp.length==10){
		key1= temp[0].toString().trim();
		Rule1= checkAlpha(temp[1])+ " " +checkAlpha(temp[2]) ;
		Rule2 = checkAlpha(temp[2])+ " " +checkDigit(temp[8]);
		Rule3 = checkSoundex(temp[1])+" " +checkSoundex(temp[2])+ " " +checkDigit(temp[8]);
		Rule4 = checkSoundex(temp[1])+" " +checkSoundex(temp[2])+" " + checkAlphaNumeric(temp[4]);
		context.write(new Text(Rule1), new Text(key1));
		context.write(new Text(Rule2), new Text(key1));
		context.write(new Text(Rule3), new Text(key1));
		context.write(new Text(Rule4), new Text(key1));
		}
		else {
			System.out.println("This line was not accepted: " + temp[0].toString());
		}
	    }
	    public static String checkAlpha(String in) {
	   	 if(in.length()==0 || in.matches("\\d+")) 
	   	 {
	   	 return key1.trim();
	   	 }
	   	 else {
	   	 return in.replaceAll("[^A-Za-z]+", "").toUpperCase().trim();
	   	 }
	    }

	    public static String checkAlphaNumeric(String in) {
	   	 if(in.length() ==0 ) 
	   	 {
	   	 return key1.trim();
	   	 }
	   	 else {
	   	 return in.replaceAll("[^A-Za-z0-9]+", "").toUpperCase().trim();
	   	 }
	    }
	    public static String checkDigit(String in) {
	   	 if(in.length() ==0 || in.matches("[A-Za-z]+")) 
	   	 {
	   	 return key1.trim();
	   	 }
	   	 else {
	   	 return in.replaceAll("\\D+", "").trim();
	   	 }
	    }
	    public static String checkSoundex(String in) {
	   	 if(in.length() ==0 || in.matches("\\d+")) 
	   	 {
	   	 return key1.trim();
	   	 }
	   	 else {
	   	 return sdx.soundex(in.toUpperCase().trim());
	   	 }
	    }
	    
	}
	public static class Assign extends Reducer<Text, Text, Text, Text>{
		Text  valueToEmit = new Text();
		Text keytoemit = new Text();
		protected void reduce(Text key, Iterable<Text> value, Context context)throws IOException, InterruptedException {
			List<String> a = new ArrayList<String>();
//			StringBuilder sb = new StringBuilder();
			for(Text vl:value) {
				String s[]=vl.toString().split("	");
				
				for(String d:s) {
					a.add(d.trim());
				}
				
//				sb.append(vl.toString()).append(",");;
		}
			Collections.sort(a);
			keytoemit.set(a.get(0).replaceAll("[\\[\\]]", "").trim());
			for(int i=0; i<a.size();i++) {
				valueToEmit.set(a.get(i).replaceAll("[\\[\\]]", "").trim());
				context.write(keytoemit, valueToEmit);
			}
			 
		}
		}


	public static void main(String[] args) throws Exception {
	    Configuration conf = new Configuration();
	    Job job = Job.getInstance(conf, "mapandreduce");
	    job.setJarByClass(prep.class);
	    job.setMapperClass(Regex.class);
	    job.setCombinerClass(Assign.class);
	    job.setReducerClass(Assign.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
}
