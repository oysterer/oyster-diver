package sortingclosure;


import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;




public class PairClosureTest {

	public static class TokenizerMapper1
    extends Mapper<Object, Text, Pair, Text>{
	    
		@Override
	    public void map(Object key, Text value, Context context) 
		 throws IOException, InterruptedException {
     StringTokenizer itr = new StringTokenizer(value.toString(), ",");
     while (itr.hasMoreTokens()) {
         Pair pr = new Pair();
         Pair rv = new Pair();
    	 pr.setKey(itr.nextToken());
    	 pr.setValue(itr.nextToken());
     context.write(pr, new Text(pr.getValue()));
     rv= pr.reversePair();
     context.write(rv, new Text(rv.getValue()));

     		}
	    }
	}

	
	public static class SortClosure1 extends Reducer<Pair, Text, Pair, Text>{
		public static int iterationcount=1;
		private Text result = new Text();

		TreeSet<Pair> newTree = new TreeSet<Pair>();
		@Override
		public void reduce(Pair source, Iterable<Text> value, Context context) throws IOException, InterruptedException {
			
			
			
			Iterator<Text> itr = value.iterator();
//			Text cur= new Text();
			String cur="";
//			String cur= "";
//			String first="";
			System.out.println("Reduce-Iteration:" + iterationcount++);
			int i=1;
//			while(itr.hasNext()) {
//				Text val = itr.next();
//				System.out.println("Reduce Key:" + source.getKey() + "Reduce Value:" + val.toString() + "Counts:" + i++);
//				context.write(source, val);
//				
//			}
			
			boolean locmaxstate = false;

//			int i=1;	    
//				String prevKey="";
//				String key="";
				String first = source.getValue();
				String firstid = source.getKey();


				System.out.println("Key:" + source.toString() + "Value: " +first.toString() + i++);
				int cmp = source.getKey().compareTo(source.getValue());

				if(cmp < 0) {
				locmaxstate=true;
				System.out.println("If Condition: " + cmp);
				newTree.add(source);
				context.write(source, new Text(first));
				}
				String lastid = firstid;
				int tracker=0;
				System.out.println("Tracker: " + tracker++);
				while(itr.hasNext()) {
					
				cur = itr.next().toString();
				if(cur == lastid)
				{
					continue;
				}
				if(locmaxstate == true) {
				    System.out.println("Key1 :" + source.getKey() + "Value1: " + cur);
					newTree.add(source);
				    context.write(source, new Text(cur));
				}
				else {
					Pair s = new Pair();
					s.setKey(first);
					s.setValue(cur);
					newTree.add(s);
					newTree.add(s.reversePair());
					System.out.println("Merge Pair:" + s.toString() + "Reverse Pair:" + s.reversePair().toString());
					context.write(new Pair(first, cur), new Text(cur));
					context.write(new Pair(cur, first), new Text(first));
				}
				lastid = source.getKey();
				int cmp2 = source.getKey().compareTo(lastid);
				if(locmaxstate  == true && cmp2 <0) {
					Pair n = new Pair();
					n.setKey(source.getKey());
					n.setValue(first);
					System.out.println("Last Pair:" + n.toString());
					newTree.add(n);
		    		context.write(source, new Text(first));
		    		}

			}

//				Iterator<Pair> outItr = newTree.iterator();
//				while(outItr.hasNext()){
//					Pair outPair= outItr.next();
//					context.write(outPair, new Text(outPair.getValue()));
//				}
				
		}	
	}
		
	
	
	public static class FirstPartitioner extends Partitioner <Pair, Text> {
		
		
		
		@Override
		 public int getPartition(Pair ky,  Text vl, int numofpartitions) {
			int part = Math.abs(ky.getKey().hashCode() % numofpartitions);
//			System.out.println("Partition" + part);
			return part;
			
		}
		
	}
	
public static class KeyComparator extends WritableComparator {
		protected KeyComparator() {
			super(Pair.class, true);
		}
		@SuppressWarnings("rawtypes")
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			Pair wc1 =(Pair) w1;
			Pair wc2 =(Pair) w2;
			int result = wc1.getKey().compareTo(wc2.getKey());
			System.out.println("Key Comparator:" + result);
			if(0 == result) {
				result = -1* wc1.getValue().compareTo(wc2.getValue());
			}
			return result;
		}
	}
	
	public static class GroupingComparator extends WritableComparator {
		
		protected GroupingComparator() {
			super(Pair.class, true); }
		
		@SuppressWarnings("rawtypes")
		@Override
		public int compare(WritableComparable pair, WritableComparable pair2) {
			Pair wc = (Pair) pair;
			Pair wc2 = (Pair) pair2;
			
			int result= wc.getKey().compareTo(wc2.getKey());
//			System.out.println("Grouping Comparator: " +result+ " wc1:" + wc.getKey() + "wc2:" + wc2.getKey()) ;
			return result;
//			return compare(wc, wc2);
	}
	
	}
	
	

	public static void main(String[] args) throws Exception {
	    Configuration conf = new Configuration();
	    Job job = Job.getInstance(conf, "mapandreduce");
	    job.setJarByClass(PairClosure.class);
	    
	    job.setPartitionerClass(FirstPartitioner.class);
	    job.setGroupingComparatorClass(GroupingComparator.class);
//	    job.setNumReduceTasks(3);
	    job.setSortComparatorClass(KeyComparator.class);

	    job.setMapOutputKeyClass(Pair.class);
	    job.setMapOutputValueClass(Text.class);
	    
	    
	   
	    job.setOutputKeyClass(Pair.class);
	    job.setOutputValueClass(Text.class);
	    
	    job.setMapperClass(TokenizerMapper1.class);
	    job.setReducerClass(SortClosure1.class);
	    
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
	
	}




