package sortingclosure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

//Comment in GIT

public class prep {
	public static class TokenizerMapper
    extends Mapper<Object, Text, Text, Text>{
		private Text ky = new Text();
	    private Text val = new Text();
	    public void map(Object key, Text value, Context context) 
		 throws IOException, InterruptedException {
     StringTokenizer itr = new StringTokenizer(value.toString(), "	");
     while (itr.hasMoreTokens()) {
     ky.set(itr.nextToken());
     val.set(itr.nextToken());
     context.write(val, ky);
     		}
	    }
	}
	public static class Sort extends Reducer<Text, Text, Text, Text>{
		Text  valueToEmit = new Text();
		Text keytoemit = new Text();
		protected void reduce(Text key, Iterable<Text> value, Context context)throws IOException, InterruptedException {
			List<String> a = new ArrayList<String>();
//			StringBuilder sb = new StringBuilder();
			for(Text vl:value) {
				String s[]=vl.toString().split("	");
				
				for(String d:s) {
					a.add(d.trim());
				}
				
//				sb.append(vl.toString()).append(",");;
		}
			Collections.sort(a);
			keytoemit.set(a.get(0).replaceAll("[\\[\\]]", "").trim());
			for(int i=0; i<a.size();i++) {
				valueToEmit.set(a.get(i).replaceAll("[\\[\\]]", "").trim());
				context.write(keytoemit, valueToEmit);
			}
			 
		}
		}


	public static void main(String[] args) throws Exception {
	    Configuration conf = new Configuration();
	    Job job = Job.getInstance(conf, "mapandreduce");
	    job.setJarByClass(prep.class);
	    job.setMapperClass(TokenizerMapper.class);
	    job.setCombinerClass(Sort.class);
	    job.setReducerClass(Sort.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
}
