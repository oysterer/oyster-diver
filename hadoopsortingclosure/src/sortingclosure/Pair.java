package sortingclosure;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

public class Pair implements Writable, WritableComparable<Pair> {
String key, value;
	
	public Pair(){
	}
	
	public Pair(String s1, String s2){
		key = s1;
		value = s2;
	}
	
	public void setKey(String in){
		key = in;
	}
	public void setValue(String in){
		value = in;
	}
	public String getKey(){
		return key;
	}
	public String getValue(){
		return value;
	}
	
	public int compareTo(Pair cp){
//		if(cp == null) return 0;
		int result = key.compareTo(cp.getKey());
		if (result==0) {
			return value.compareTo(cp.getValue());
		} else {
			return result;
		}
	}
	@Override
	public String toString() {
	return key.toString() + " " + value.toString();
	}
	
	public boolean isEqualTo(Pair pr){
		if ((this.key).equals(pr.getKey()) && (this.value).equals(pr.getValue())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean hasSameKeyAs(Pair pr){
		if ((this.key).equals(pr.getKey())) {
			return true;
		} else {
			return false;
		}	
	}
	
	public Pair reversePair(){
		Pair result = new Pair();
		result.setKey(value);
		result.setValue(key);
		return result;
	}
	
	public Boolean keySameAsValue(){
		if(key.equals(value)) return true; else return false;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.key = WritableUtils.readString(in);
		this.value = WritableUtils.readString(in);
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		WritableUtils.writeString(out, key);
		WritableUtils.writeString(out, value);		
	}

//	public  int compare(Pair cp, Pair cp2) {
//		if(cp == null) return 0;
//		int result = cp.getKey().compareTo(cp2.getKey());
//		if (result==0) {
//			return cp.getValue().compareTo(cp2.getValue());
//		} else {
//			return result;
//		}
//		
//	}


	
}
