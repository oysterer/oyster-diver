package sortingclosure;


import java.awt.List;
import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeSet;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;



public class PairClosure {

	/*public static class TokenizerMapper
    extends Mapper<Object, Text, Text, List<Pair>>{
	
	    public void map(Object key, Text value, Context context) 
		 throws IOException, InterruptedException {
     StringTokenizer itr = new StringTokenizer(value.toString(), ",");
		List tree = new List<Pair>();
     while (itr.hasMoreTokens()) {
    	 Pair pr = new Pair();
    	 pr.setKey(itr.nextToken());
     pr.setValue(itr.nextToken());
     tree.add(pr);
     		}
     context.write(new Text("Hello"), tree);
	    }
	}

	
	public static class SortClosure extends Reducer<Text, List<Pair>, Text, Text>{
		
		protected void reduce(Text currkey, TreeSet<Pair> tree, Context context)throws IOException, InterruptedException {
			TreeSet<Pair> newTree = new TreeSet<Pair>();
			Iterator<Pair> itr = tree.iterator();
			while (itr.hasNext()){
				Pair pair = itr.next();
				newTree.add(pair);
				newTree.add(pair.reversePair());
			}
		
			
			tree.clear();
			tree.addAll(newTree);
			newTree.clear();
			TreeSet<Pair> keyGroup = new TreeSet<Pair>();
			boolean allGroupsClosed=false;
			while (!allGroupsClosed) {
				String prevKey = "";
				String key = "";
				allGroupsClosed = true;
				itr = tree.iterator();
				while(itr.hasNext()){
					Pair currPair = itr.next();
					// get key of current pair being processed
					key = currPair.getKey();
					if (!key.equals(prevKey) || !itr.hasNext()) {
						if (key.equals(prevKey) && !itr.hasNext()) keyGroup.add(currPair);
						if (!key.equals(prevKey) && !itr.hasNext() && (key.compareTo(currPair.getValue())<=0)) newTree.add(currPair);
						 System.out.println("cKey "+key+" pKey "+prevKey+ " "+ keyGroup.size());
						if (!prevKey.equals("")) {
							Pair firstPair = keyGroup.first();
							String firstKey = firstPair.getKey();
							String firstValue = firstPair.getValue();
							if (firstKey.compareTo(firstValue) > 0) {
								if (keyGroup.size() > 1) {
									allGroupsClosed = false;
									
									Iterator<Pair> grpItr = keyGroup.iterator();
									Pair grpPair = grpItr.next();
									while (grpItr.hasNext()) {
										grpPair = grpItr.next();
										Pair newPair = new Pair();
										newPair.setKey(firstValue);
										newPair.setValue(grpPair.getValue());
										newTree.add(newPair);
										newTree.add(newPair.reversePair());
									}
									// Decide if first pair should be added
									grpPair = keyGroup.last();
									String lastValue = grpPair.getValue();
									if (firstKey.compareTo(lastValue)<0) {
										newTree.add(firstPair);
									}
								}
							} else {
								// First pair not reversed, move key group as is to newTree
								newTree.addAll(keyGroup);
									}
				}
						keyGroup.clear();

			}
					keyGroup.add(currPair);
					// change previous key to current key for next pair to process
					prevKey = key;
			
			
	        }
				tree.clear();
				tree.addAll(newTree);
				newTree.clear();
				
			}
			Iterator<Pair> outItr = tree.iterator();
			while(outItr.hasNext()){
				Pair outPair= outItr.next();
				context.write(new Text(outPair.getKey()), new Text(outPair.getValue()));
			}
		}
	}

	public static void main(String[] args) throws Exception {
	    Configuration conf = new Configuration();
	    Job job = Job.getInstance(conf, "mapandreduce");
	    job.setJarByClass(PairClosure.class);
	    job.setMapperClass(TokenizerMapper.class);
	    job.setCombinerClass(SortClosure.class);
	    job.setReducerClass(SortClosure.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(TreeSet.class);
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
	*/
	}




