package sortingclosure;


import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;




public class PairClosureTest2 {
	
	public enum IterationStats {
	    changes
	}

	public static class TokenizerMapper2
    extends Mapper<Object, Text, Pair, Text>{
	    
		@Override
	    public void map(Object key, Text value, Context context) 
		 throws IOException, InterruptedException {
     StringTokenizer itr = new StringTokenizer(value.toString(), ",");
     while (itr.hasMoreTokens()) {
         Pair pr = new Pair();
         Pair rv = new Pair();
    	 pr.setKey(itr.nextToken());
    	 pr.setValue(itr.nextToken());
     context.write(pr, new Text(pr.getValue()));
     rv= pr.reversePair();
     context.write(rv, new Text(rv.getValue()));

     		}
	    }
	}

	
	public static class SortClosure2 extends Reducer<Pair, Text, Text, Text>{
		public static int iterationcount=1;
		private Text result = new Text();
		

		TreeSet<Pair> newTree = new TreeSet<Pair>();
		@Override
		public void reduce(Pair source, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			
			// Part 1 of Reducer, load key group into TreeSet
			// This will sort the pairs by value, and remove duplicate pairs
			TreeSet<Pair> tree = new TreeSet<Pair>();
//				Iterator<Text> itr = values.iterator();
//			 // Load the tree with pairs in the key group
			 // Convert m/r Text to Java String type
		     for (Text val : values) {
				Pair pair = new Pair();
				pair.setKey(source.getKey());
				pair.setValue(val.toString());
				tree.add(pair);
		     }
			 
			// Part 2 of Reducer
			 // pairs are now sorted by value in the tree
			 // Start by getting first pair in the tree
			 boolean allGroupsClosed = false;
			 Text t1 = new Text();
			 Text t2 = new Text();
			 
			 allGroupsClosed = true;
		     Pair firstPair = source;
			 String firstKey = firstPair.getKey();
			 String firstValue = firstPair.getValue();
			 // if key and value of first pair is out of order, generate new pairs
			 System.out.println("Tree Size:" +tree.size() +" Key and Value out of order: " + firstKey.compareTo(firstValue) + "Reduce Iteration:" +iterationcount++);
			 
			 if (firstKey.compareTo(firstValue) > 0) {
				if(tree.size() > 1) {
					// if both conditions are true, closure is not finished
					allGroupsClosed = false;
					Iterator<Pair> grpItr = tree.iterator();
					// pull out first pair in tree so loop starts with second pair
					Pair nextPair = grpItr.next();
					while (grpItr.hasNext()) {
						nextPair = grpItr.next();
						// Convert Java String to m/r Text
						t1.set(firstValue);
						t2.set(nextPair.getValue());
						System.out.println("In Merge State, First Value:" + t1.toString()+ "Next Pair:" +  t2.toString());
						context.write(t1, t2);
						context.write(t2, t1);
					}
					String lastValue = (tree.last()).getValue();
					if (firstKey.compareTo(lastValue)<0) {
						t1.set(firstValue);
						t2.set(lastValue);
						System.out.println("Last Key and Value:" + firstValue + lastValue);
						context.write(t1, t2);
					}
				}
			} else {	
				// Pass forward all pairs in key group without change
				Iterator<Pair> grpItr = tree.iterator();
				while (grpItr.hasNext()) {
					Pair nextPair = grpItr.next();
					// Convert Java String to m/r Text
					String key, val;
					key= nextPair.getKey();
					val= nextPair.getValue();
					System.out.println("Forward Pass: Key:" +key+"Value:" + val);
					t1.set(key);
					t2.set(val);
					context.write(t1, t2);
				}
			}	
//			 context.getCounter(IterationStats.changes).increment(1);

			 
	}
	}

		
	
	
	public static class SecondPartitioner extends Partitioner <Pair, Text> {
		
		
		
		@Override
		 public int getPartition(Pair ky,  Text vl, int numofpartitions) {
			int part = Math.abs(ky.getKey().hashCode() % numofpartitions);
//			System.out.println("Partition" + part);
			return part;
			
		}
		
	}
	
public static class KeyComparator1 extends WritableComparator {
		protected KeyComparator1() {
			super(Pair.class, true);
		}
		@SuppressWarnings("rawtypes")
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			Pair wc1 =(Pair) w1;
			Pair wc2 =(Pair) w2;
			int result = wc1.getKey().compareTo(wc2.getKey());
//			System.out.println("Key Comparator:" + result);
			if(0 == result) {
				result = 1* wc1.getValue().compareTo(wc2.getValue());
			}
			return result;
		}
	}
	
	public static class GroupingComparator1 extends WritableComparator {
		
		protected GroupingComparator1() {
			super(Pair.class, true); }
		
		@SuppressWarnings("rawtypes")
		@Override
		public int compare(WritableComparable pair, WritableComparable pair2) {
			Pair wc = (Pair) pair;
			Pair wc2 = (Pair) pair2;
			
			int result= wc.getKey().compareTo(wc2.getKey());
//			System.out.println("Grouping Comparator: " +result+ " wc1:" + wc.getKey() + "wc2:" + wc2.getKey()) ;
			return result;
//			return compare(wc, wc2);
	}
	
	}
	
	

	public static void main(String[] args) throws Exception {
	    Configuration conf = new Configuration();
	    Job job = Job.getInstance(conf, "mapandreduce");
	    job.setJarByClass(PairClosureTest2.class);
	    
	    job.setPartitionerClass(SecondPartitioner.class);
	    job.setGroupingComparatorClass(GroupingComparator1.class);
//	    job.setNumReduceTasks(2);
//	    job.setSortComparatorClass(KeyComparator1.class);

	    job.setMapOutputKeyClass(Pair.class);
	    job.setMapOutputValueClass(Text.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    job.setMapperClass(TokenizerMapper2.class);
	    job.setReducerClass(SortClosure2.class);
	    
//	    job.setSortComparatorClass(KeyComparator.class);
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));

	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
	
	}




