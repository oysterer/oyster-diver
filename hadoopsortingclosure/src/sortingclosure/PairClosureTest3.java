package sortingclosure;


import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;




public class PairClosureTest3 {
	
	public enum IterationStats {
	    changes
	}

	public static class TokenizerMapper3
    extends Mapper<Object, Text, NullWritable, Pair>{
	    
		@Override
	    public void map(Object key, Text value, Context context) 
		 throws IOException, InterruptedException {
//			ArrayWritable arrayWritable = new ArrayWritable(Pair.class);
     StringTokenizer itr = new StringTokenizer(value.toString(), "	");
     while (itr.hasMoreTokens()) {
         Pair pr = new Pair();
    	 pr.setKey(itr.nextToken());
    	 pr.setValue(itr.nextToken());
     context.write(NullWritable.get(), pr);
     context.write(NullWritable.get(), pr.reversePair());


     		}
	    }
	}

	
	public static class SortClosure3 extends Reducer<NullWritable, Pair, Text, Text>{
		public static int iterationcount=1;
//		private Text result = new Text();
		TreeSet<Pair> OutTree = new TreeSet<Pair>();

		@Override
		public void reduce(NullWritable source, Iterable<Pair> values, Context context) throws IOException, InterruptedException {
			
			TreeSet<Pair> newTree = new TreeSet<Pair>();
			TreeSet<Pair> tree = new TreeSet<Pair>();
			
			for (Pair val : values) {
				Pair pair = new Pair();
				pair.setKey(val.getKey());
				pair.setValue(val.getValue());
				tree.add(pair);
		     }
			Iterator<Pair> itr = tree.iterator();

			TreeSet<Pair> keyGroup = new TreeSet<Pair>();
			boolean allGroupsClosed=false;
			while (!allGroupsClosed) {
				String prevKey = "";
				String key = "";
				allGroupsClosed = true;
				itr = tree.iterator();
				while(itr.hasNext()){
					Pair currPair = itr.next();
					// get key of current pair being processed
					key = currPair.getKey();
					if (!key.equals(prevKey) || !itr.hasNext()) {
						if (key.equals(prevKey) && !itr.hasNext()) keyGroup.add(currPair);
						if (!key.equals(prevKey) && !itr.hasNext() && (key.compareTo(currPair.getValue())<=0)) newTree.add(currPair);
//						 System.out.println("cKey "+key+" pKey "+prevKey+ " "+ keyGroup.size());
						if (!prevKey.equals("")) {
							Pair firstPair = keyGroup.first();
							String firstKey = firstPair.getKey();
							String firstValue = firstPair.getValue();
							if (firstKey.compareTo(firstValue) > 0) {
								if (keyGroup.size() > 1) {
									allGroupsClosed = false;
									
									Iterator<Pair> grpItr = keyGroup.iterator();
									Pair grpPair = grpItr.next();
									while (grpItr.hasNext()) {
										grpPair = grpItr.next();
										Pair newPair = new Pair();
										newPair.setKey(firstValue);
										newPair.setValue(grpPair.getValue());
										newTree.add(newPair);
										newTree.add(newPair.reversePair());
									}
									// Decide if first pair should be added
									grpPair = keyGroup.last();
									String lastValue = grpPair.getValue();
									if (firstKey.compareTo(lastValue)<0) {
										newTree.add(firstPair);
									}
								}
							} else {
								// First pair not reversed, move key group as is to newTree
								newTree.addAll(keyGroup);
									}
				}
						keyGroup.clear();

			}
					keyGroup.add(currPair);
					// change previous key to current key for next pair to process
					prevKey = key;
			
			
	        }
				tree.clear();
				tree.addAll(newTree);
				newTree.clear();
				
			}
			Iterator<Pair> outItr = tree.iterator();
			while(outItr.hasNext()){
				Pair outPair= outItr.next();
				context.write(new Text(outPair.getKey()), new Text(outPair.getValue()));
			}
			 
	}
	}

		
	
	
//	public static class SecondPartitioner extends Partitioner <Pair, Text> {
//		
//		
//		
//		@Override
//		 public int getPartition(Pair ky,  Text vl, int numofpartitions) {
//			int part = Math.abs(ky.getKey().hashCode() % numofpartitions);
////			System.out.println("Partition" + part);
//			return part;
//			
//		}
//		
//	}
//	
//public static class KeyComparator1 extends WritableComparator {
//		protected KeyComparator1() {
//			super(Pair.class, true);
//		}
//		@SuppressWarnings("rawtypes")
//		@Override
//		public int compare(WritableComparable w1, WritableComparable w2) {
//			Pair wc1 =(Pair) w1;
//			Pair wc2 =(Pair) w2;
//			int result = wc1.getKey().compareTo(wc2.getKey());
////			System.out.println("Key Comparator:" + result);
//			if(0 == result) {
//				result = 1* wc1.getValue().compareTo(wc2.getValue());
//			}
//			return result;
//		}
//	}
//	
//	public static class GroupingComparator1 extends WritableComparator {
//		
//		protected GroupingComparator1() {
//			super(Pair.class, true); }
//		
//		@SuppressWarnings("rawtypes")
//		@Override
//		public int compare(WritableComparable pair, WritableComparable pair2) {
//			Pair wc = (Pair) pair;
//			Pair wc2 = (Pair) pair2;
//			
//			int result= wc.getKey().compareTo(wc2.getKey());
////			System.out.println("Grouping Comparator: " +result+ " wc1:" + wc.getKey() + "wc2:" + wc2.getKey()) ;
//			return result;
////			return compare(wc, wc2);
//	}
//	
//	}
	
	

	public static void main(String[] args) throws Exception {
	    Configuration conf = new Configuration();
	    Job job = Job.getInstance(conf, "mapandreduce");
	    job.setJarByClass(PairClosureTest3.class);
	    
//	    job.setPartitionerClass(SecondPartitioner2.class);
//	    job.setGroupingComparatorClass(GroupingComparator2.class);
//	    job.setNumReduceTasks(2);
//	    job.setSortComparatorClass(KeyComparator1.class);

	    job.setMapOutputKeyClass(NullWritable.class);
	    job.setMapOutputValueClass(Pair.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    job.setMapperClass(TokenizerMapper3.class);
	    job.setReducerClass(SortClosure3.class);
	    
//	    job.setSortComparatorClass(KeyComparator.class);
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));

	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
	
	}




